<?php
// Routes page
//require 'Server.php';
//require 'User.php';
require 'IStockSessionManager.php';

use Parse\ParseUser;
use Parse\ParseClient;
use Parse\ParseSessionStorage;

$server = new Server();

$app->get('/[{name}]', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->get('/reset/', function ($request, $response, $args) {
    return $this->renderer->render($response, 'reset.phtml', $args);
});

$app->get('/cancelReset/', function ($request, $response, $args) {
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->get('/emailReset/', function ($request, $response, $args) {
    $email = $request->getParam('emailInput');
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->get('/logout/', function ($request, $response, $args) {
    ParseUser::logOut();
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->post('/loginDashboard/', function ($request, $response, $args) {
    // Sample log message
    $username = $request->getParam('usernameInput');
    $password = $request->getParam('passwordInput');
    try {
        // error_log("Logging in", 4);
        ParseUser::logIn($username, $password);
        $_SESSION['user'] = ParseUser::getCurrentUser();
        // error_log("Logged in", 4);
        // error_log(print_r($_SESSION, true), 4);
        $page = "stock.phtml";
        $data = "Login success";

        
    } catch (Exception $error) {
        $page = "index.phtml";
        $data = "Login Failed";
        echo ("<script> 
            alert('Username/password combination not found.');      
        </script>");
    }
    echo ("<script>console.log('PHP: ".$data."'); </script>");
    return $this->renderer->render($response, $page, $args);
    // Render index view
    
});

$app->get('/accountInfo/', function($request, $response, $args) {

    $user = $_SESSION['user'];
    error_log(print_r($_SESSION, true), 4);

    $responseObject['accountBalance'] = $user->get("accountBalance");
    //$responseObject['portfolioValue'] = $user->get("portfolioValue"); no longer held in database


    $response = $response->withHeader('Content-Type', 'application/json');
    $response->write(json_encode($responseObject));

});
