<?php
// User Class

require 'Stock.php';

class User {
	public $email = "";
	public $password = "";
	public $balance = 0.0;
	public $portfolio = "";
	public $watchlist = "";

	public function setEmail($val) { $this->email = $val;}
	public function setPassword($val) { $this->password = $val;}
	public function setBalance($val) { $this->balance = $val;}
	public function setPortfolio($val) { $this->portfolio = $val;}
	public function setWatchlist($val) { $this->watchlist = $val;}

	public function getEmail() { return $this->email; }
	public function getPassword() { return $this->password; }
	public function getBalance() { return $this->balance; }
	public function getPortfolio() { return $this->portfolio; }
	public function getWatchlist() { return $this->watchlist; }

	public function addToPortfolio() {

	}

	public function addToWatchlist() {
		
	}

}
?>
