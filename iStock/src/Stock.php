<?php
// Stock Class

class Stock {
	public $ticker = "";
	public $companyName = "";
	public $currentPrice = 0.0;

	public function setTicker($val) { $this->ticker = $val;}
	public function setCompanyName($val) { $this->companyName = $val;}
	public function setCurrentPrice($val) { $this->currentPrice = $val;}

	public function getTicker() { return $this->ticker; }
	public function getCompanyName() { return $this->companyName; }
	public function getCurrentPrice() { return $this->currentPrice; }
}
?>
