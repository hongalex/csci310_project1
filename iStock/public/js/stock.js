// updates the page when first loaded - basically use all methods here lol
$(document).ready(function()
{
	$.get("/accountInfo/", function(response)
	{
		// response = JSON.parse(response);

		// $("#account_bal").append("<br>$"+response.accountBalance);
		//$("#portfolio_value").append("<br>$"+response.portfolioValue);

		var tickers = [
					{ticker:'aapl', qty: 1000},
					{ticker:'goog', qty: 500},
					{ticker:'wes', qty: 100},
				];
		tickers.forEach(loadStocks);
	});

		
	
	updateTime();
	refreshPage();
});

function loadStocks(ticker) {
	$.ajax({
		url: 'http://dev.markitondemand.com/MODApis/Api/v2/Quote/jsonp?symbol=' + ticker.ticker,
		dataType: 'jsonp',
		success: function(data) {
			data.qty = ticker.qty;
			var stockTableRow = createTableRow(data);
			$("#portfolio_table").append(stockTableRow);
			// console.log(data);
			// $("body").append("Price: "+ data.lastPrice);
		}
	});
}

function createTableRow(data) {
	var stock = $("<tr></tr>");
	stock.append("<td>"+data.Symbol+"</td>");
	stock.append("<td>"+data.Name+"</td>");
	stock.append("<td>"+data.qty+"</td>");
	stock.append("<td>"+data.LastPrice+"</td>");
	stock.append("<td>"+<input type="checkbox" name="vehicle" value="Bike"> 
  	<input type="submit" value="Submit">+"</td>");

	return stock;
}

function refreshPage() {
	//updateTime();
	updateGraph();

}

//TODO: adjust Date() object (now) to look right - check documentation
function updateTime() {
	var now = new Date();
	$('#time_date').html(now.toString());

}

setInterval(updateTime, 10000); // 10 secs



function updateGraph() {

}
